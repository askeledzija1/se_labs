# ukljucivanje biblioteke pygame
import pygame



def next_bg(color):
    if color == POZADINA:
        color = RED
    elif color == RED:
        color = GREEN
    elif color ==  GREEN:
        color = BLUE
    elif color ==  BLUE:
        color = POZADINA
    return color

def img_next_bg(slika):
    if slika == bg_img1:
        slika = bg_img2
    elif slika == bg_img2:
        slika = bg_img3
    elif slika ==  bg_img3:
        slika = bg_img4
    elif slika == bg_img4:
        slika = bg_img5
    elif slika == bg_img5:
        slika =bg_img1
    return slika

def img_next_text(text):
    if text == text1:
        text = text2
    elif text == text2:
        text = text3
    elif text ==  text3:
        text = text4
    elif text == text4:
        text = text5
    elif text == text5:
        text = text1
    return text

pygame.init()
pygame.font.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900

RED = [255,0,0]
GREEN = [0,255,0]
BLUE = [0,0,255]
POZADINA = [20,83,178]

bg_img1 = pygame.image.load("zoe1.jpg")
bg_img1 = pygame.transform.scale(bg_img1, (WIDTH-100, HEIGHT-100))
bg_img2 = pygame.image.load("zoe2.jpg")
bg_img2 = pygame.transform.scale(bg_img2, (WIDTH-100, HEIGHT-100))
bg_img3 = pygame.image.load("zoe3.jpg")
bg_img3 = pygame.transform.scale(bg_img3, (WIDTH-100, HEIGHT-100))
bg_img4 = pygame.image.load("zoe4.jpg")
bg_img4 = pygame.transform.scale(bg_img4, (WIDTH-100, HEIGHT-100))
bg_img5 = pygame.image.load("mercedes.jpeg")
bg_img5 = pygame.transform.scale(bg_img5, (WIDTH-100, HEIGHT-100))

myfont = pygame.font.SysFont('Arial', 40)
text1 = myfont.render("Ovo je Zoe Deutch...", False, RED)
text2 = myfont.render("Ona je lijepa...", False, RED)
text3 = myfont.render("Ona je glumica...", False, RED)
text4 = myfont.render("Cak joj dobro stoje i naocale...", False, RED)
text5 = myfont.render("Za kraj jedan mercedes...", False, RED)

# tuple velicine prozora
size = (WIDTH, HEIGHT)

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Igra")

clock = pygame.time.Clock()
i = 0
duration = 10000
bg_color = POZADINA
bg_img = bg_img1
text = text1
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                bg_color = next_bg(bg_color)
    i += 1
    if duration < 0:
        duration = 10000
        bg_color = next_bg(bg_color)
        bg_img = img_next_bg(bg_img)
        text = img_next_text(text)

    screen.fill(bg_color)
    screen.blit(bg_img, (0,0))
    screen.blit(text, (0, 0))
    pygame.display.flip()
    
    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
    duration = duration-time
    #print(duration)
pygame.quit()