# ukljucivanje biblioteke pygame
import pygame


pygame.init()
pygame.font.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900
transparentno = (0, 0, 0, 0)
crna = (0, 0, 0)
RED = [ 255, 0, 0 ]
WHITE = (255, 255, 255)
plava = (135, 206, 250)

#ucitavanje slika za play1
bg_img = pygame.image.load("pozadina.jpg")
bg_img = pygame.transform.scale(bg_img, (WIDTH, HEIGHT-50))
prva_slika = pygame.image.load("prsten.png")
prva_slika = pygame.transform.scale(prva_slika, (30, 30))
prva_slika_copy = pygame.transform.scale(prva_slika, (30, 30))
druga_slika = pygame.image.load("gumica.png")
druga_slika = pygame.transform.scale(druga_slika, (100, 100))
druga_slika_copy = pygame.transform.scale(druga_slika, (100, 100))
treca_slika = pygame.image.load("naljepnice.png")
treca_slika = pygame.transform.scale(treca_slika, (100, 100))
treca_slika_copy = pygame.transform.scale(treca_slika, (100, 100))
cetvrta_slika = pygame.image.load("gucci.png")
cetvrta_slika = pygame.transform.scale(cetvrta_slika, (100, 100))
cetvrta_slika_copy = pygame.transform.scale(cetvrta_slika, (100, 100))

#ucitavanje slika za play2
bg_img2 = pygame.image.load("neuredna_soba.jpg")
bg_img2 = pygame.transform.scale(bg_img2, (WIDTH, HEIGHT-50))
potty = pygame.image.load("potty.png")
potty = pygame.transform.scale(potty, (126, 187))
potty_copy = pygame.transform.scale(potty, (126, 187))
watch = pygame.image.load("clock.png")
watch = pygame.transform.scale(watch, (111, 109))
watch_copy = pygame.transform.scale(watch, (111, 109))
toy = pygame.image.load("toy.png")
toy = pygame.transform.scale(toy, (64, 42))
toy_copy = pygame.transform.scale(toy, (64, 42))
table = pygame.image.load("table.png")
table_copy = pygame.image.load("table.png")
buzz = pygame.image.load("buzz.png")
buzz = pygame.transform.scale(buzz, (50, 50))
buzz_copy = pygame.transform.scale(buzz, (50, 50))

#transformacija slika za obrub play2
potty_trans = pygame.transform.scale(potty, (50, 50))
potty_trans_copy = pygame.transform.scale(potty, (50, 50))
watch_trans = pygame.transform.scale(watch, (50, 50))
watch_trans_copy = pygame.transform.scale(watch, (50, 50))
toy_trans = pygame.transform.scale(toy, (50, 50))
toy_trans_copy = pygame.transform.scale(toy, (50, 50))
table_trans = pygame.transform.scale(table, (50, 50))
table_trans_copy = pygame.transform.scale(table, (50, 50))
buzz_trans = pygame.transform.scale(buzz, (50, 50))
buzz_trans_copy = pygame.transform.scale(buzz, (50, 50))


#transformacija slika za obrub play1
prva_trans = pygame.transform.scale(prva_slika, (50, 50))
prva_trans_copy = pygame.transform.scale(prva_slika, (50, 50))
druga_trans = pygame.transform.scale(druga_slika, (50, 50))
druga_trans_copy = pygame.transform.scale(druga_slika, (50, 50))
treca_trans = pygame.transform.scale(treca_slika, (50, 50))
treca_trans_copy = pygame.transform.scale(treca_slika, (50, 50))
cetvrta_trans = pygame.transform.scale(cetvrta_slika, (50, 50))
cetvrta_trans_copy = pygame.transform.scale(cetvrta_slika, (50, 50))



# tuple velicine prozora
size = (WIDTH, HEIGHT)

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Detektivska Igra")

clock = pygame.time.Clock()
#fontovi
myfont = pygame.font.SysFont('Arial', 20)
timefont = pygame.font.SysFont('Arial', 24)
start_text = myfont.render("Za razinu Radni stol, stisnite F1 ! ", False, RED)
start_text2 = myfont.render("Za razinu Neuredna soba, stisnite F2 ! ", False, RED)
play_text = myfont.render("Preostali objekti:", False, RED)

state = "start"
duration = 30000



clock = pygame.time.Clock()









done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            
            # if event.key == pygame.K_SPACE:
            #     if state == "start":
            if event.key == pygame.K_F2:
                if state == "start":
                    state = "play2"
                    
            if event.key == pygame.K_F1:
                 if state == "start":
                     state = "play1"
                    
                
                    
        if event.type == pygame.MOUSEBUTTONDOWN:
            if state == "play1":
                
                
                if pos1.collidepoint(event.pos):
                    prva_slika_copy.fill(transparentno)
                    prva_trans_copy.fill(crna)
                    a = True
                if pos2.collidepoint(event.pos):
                    druga_slika_copy.fill(transparentno)
                    druga_trans_copy.fill(crna)
                    b = True
                if pos3.collidepoint(event.pos):
                    treca_slika_copy.fill(transparentno)
                    treca_trans_copy.fill(crna)
                    c = True
                if pos4.collidepoint(event.pos):
                    cetvrta_slika_copy.fill(transparentno)
                    cetvrta_trans_copy.fill(crna)
                    d = True
            if state == "play2":
                

                
                if pos5.collidepoint(event.pos):
                    potty_copy.fill(transparentno)
                    potty_trans_copy.fill(crna)
                    e = True
                if pos6.collidepoint(event.pos):
                    watch_copy.fill(transparentno)
                    watch_trans_copy.fill(crna)
                    f= True
                if pos7.collidepoint(event.pos):
                    toy_copy.fill(transparentno)
                    toy_trans_copy.fill(crna)
                    g = True
                if pos8.collidepoint(event.pos):
                    table_copy.fill(transparentno)
                    table_trans_copy.fill(crna)
                    h = True
                if pos9.collidepoint(event.pos):
                    buzz_copy.fill(transparentno)
                    buzz_trans_copy.fill(crna)
                    j = True
            
                
                
   #state change
    if state == "play1" or state =="play2" :
        
        
        timeleft -= time
        # if state == "score" :
        #     timeleft = timeleft
        #     vrijeme = 30000 - timeleft
            
        if timeleft <= 0 or a == True and b == True and c == True and d == True or e == True and f == True and g == True and h == True and j == True  :
           state = "score"     
           vrijeme = 30000 - timeleft
        
            

    
    #iscrtavanje
    if state == "start":
        screen.fill(crna)
        screen.blit(start_text, (30,30))
        screen.blit(start_text2, (90,90))
        timeleft = duration
        #uvjeti za score
        a = False
        b = False
        c = False
        d = False
        e = False
        f = False
        g = False
        h = False
        j = False
        #ponovno ucitavanje slika nakon nestajanja
        prva_trans_copy.blit(prva_trans, (0, 0))
        druga_trans_copy.blit(druga_trans, (0, 0))
        treca_trans_copy.blit(treca_trans, (0, 0))
        cetvrta_trans_copy.blit(cetvrta_trans, (0, 0))

        prva_slika_copy.blit(prva_slika, (0, 0))
        druga_slika_copy.blit(druga_slika, (0, 0))
        treca_slika_copy.blit(treca_slika, (0, 0))
        cetvrta_slika_copy.blit(cetvrta_slika, (0, 0))

        potty_copy.blit(potty, (0, 0))
        watch_copy.blit(watch, (0, 0))
        toy_copy.blit(toy, (0, 0))
        table_copy.blit(table, (0, 0))
        buzz_copy.blit(buzz, (0, 0))

        potty_trans_copy.blit(potty_trans, (0, 0))
        watch_trans_copy.blit(watch_trans, (0, 0))
        toy_trans_copy.blit(toy_trans, (0, 0))
        table_trans_copy.blit(table_trans, (0, 0))
        buzz_trans_copy.blit(buzz_trans, (0, 0))
    
    elif state == "play1":   
        screen.blit(prva_trans_copy, (200, 850))
        screen.blit(druga_trans_copy, (300, 850))
        screen.blit(treca_trans_copy, (400, 850))
        screen.blit(cetvrta_trans_copy, (500, 850)) 
        screen.blit(bg_img, (0,0))
        
        
        pos1 = screen.blit(prva_slika_copy, (0,820))
        pos2 = screen.blit(druga_slika_copy, (660, 750))
        pos3 = screen.blit(treca_slika_copy, (1100, 450))
        pos4 = screen.blit(cetvrta_slika_copy, (1260,720))
        time_text = timefont.render("Time: %.2f"%(timeleft/1000), False, WHITE)
        time_height = time_text.get_height()
        screen.blit(time_text, (10, 0))
        screen.blit(play_text, (0, 850))
        
    elif state == "play2":
        
        screen.blit(bg_img2, (0,0))
        pos5 = screen.blit(potty_copy, (1260, 550))
        pos6 = screen.blit(watch_copy, (1470,50))
        pos7 = screen.blit(toy_copy, (550, 425))
        pos8 = screen.blit(table_copy, (790, 550))
        pos9 = screen.blit(buzz_copy, (1175, 520))
        screen.blit(potty_trans_copy, (200, 850))
        screen.blit(watch_trans_copy, (600, 850))
        screen.blit(toy_trans_copy, (300, 850))
        screen.blit(table_trans_copy, (400, 850))
        screen.blit(buzz_trans_copy, (500, 850))
        time_text = timefont.render("Time: %.2f"%(timeleft/1000), False, crna)
        time_height = time_text.get_height()
        screen.blit(time_text, (10, 0))
        screen.blit(play_text, (0, 850))
        
        
        
    elif state == "score":
        screen.fill(plava)
        # vrijeme = 30000 - timeleft
        if vrijeme < 30000 :
           vrijeme_text = timefont.render("Cestitke, presli ste razinu za: %.2f sec"%(vrijeme/1000), False, crna)
           screen.blit(vrijeme_text, (0, 0))
           score_text = myfont.render("Pritisnite SPACE kako biste opet igrali !", False, crna)
           screen.blit(score_text, (0, 100))
        if vrijeme >= 30000 :
           vrijeme_text2 = timefont.render("Nazalost, vrijeme je isteklo !", False, crna)
           screen.blit(vrijeme_text2, (0, 0))
           score_text2 = myfont.render("Pritisnite SPACE kako biste opet igrali !", False, crna)
           screen.blit(score_text2, (0, 100))
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if state == "score":
                    state = "start"
    
    
        
        
        
        
        
    
        
    pygame.display.flip()
    
    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
    
    #print(duration)
pygame.quit()